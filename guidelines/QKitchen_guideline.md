# The guideline for QKitchen workspace

Version: *16.04.2020*

## Introduction (TL;DR)


Welcome to the guideline for the QKitchen workspace. As QWorld is an international community aiming and promoting collobarations, we ask to follow certain conventions that will be helpful not only for the developers of the projects but also the users benefiting from these projects (e.g., the educators, the mentors, or participants of the events).

Here you will find the rules and tips for developing your project on the QKitchen workspace.

See you and your results soon!

Best,  
The coordinators of QKitchen
qkitchen [at] qworld.lu.lv

## General rules

### Licensing and contribution

Every contributor must read and agree to follow [the Code of Ethics and Code of Conduct of QWorld](http://qworld.lu.lv/index.php/code-of-ethics-and-conduct/), and [the Code of Conduct for QKitchen workspace](https://gitlab.com/qkitchen/qworld-documentation/-/blob/dda01f1b5e88a324931271fd57487c86771b0e93/general/code_of_conduct/coc_qkitchen_2020_03_30.md).

QKitchen uses open-source licenses. The default licences of QKitchen are as follows:

* CC-BY-4.0 (or earlier versions) for the content, documentation, texts, graphics, and similar materials, available at https://creativecommons.org/licenses/by/4.0/legalcode, and
* Apache License 2.0 for code and software, available at http://www.apache.org/licenses/LICENSE-2.0

Your work must be licensed appropriately. Besides, any contributor must agree to provide his or her name and email address for transparency.

Please be friendly and welcoming :)

We encourage each project to welcome contributions from the world. Please give credits to the works done by the others. If the works done by the others are major or significant, then please invite them to be one of the contributors to the specific part of the peoject (e.g., a single notebook) or the whole project.

When you are interested in contributing to a project in QKitchen, please contact its owners, maintainers, or contributors. But, please keep in your mind that they have the rigths to make the final decisions about your involvements.

When you have any suggestions, proposals, questions or concerns, you can always contact the coordinators of QKitchen.

### Technical general rules

QKitchen workspace is located at GitLab repository using free and open source distributed version control system "git". A lot of resources are avialable on the internet about how to use "git".

In order to avoid accidental change of the code and to reduce unnecessary merge conflicts, a user or developer is allowed to access only one or a few branches of a project. In particular, `master` branch is protected from anyone except the maintainers of the project and the coordinators of QKitchen.

Inside any big project, there is always one main branch (`master` or `release`) on which the final or pre-final versions of the project are kepy. In order to add your contributions to a branch, you should create a merge request from your branch to the target branch.

A set of rules you are expected to follow:
* **Do not push unnecessary files to repository.** Some commands or apps may produce additional or temporary files, which might be important only for the sake of the local executions. For example, `.ipynb` files by jupyter notebooks and `.log`, `.aux`, or `.bbl` files by LaTeX compilers. The main problem here is that such files oftenly create unnecessary merge conflicts.
* **Create README.md file.** When one reaches a project on Gitlab, the content of README.md file is automatically shown. You can see it as the identification and short introduction of the project. This file should introduce the project, its contributors, its licence, how to access and use the project, and similar (preferable short) information. `README.md` should be written in a [markdown style](https://www.markdownguide.org/basic-syntax/) and HTML codes should not be used. The README.md file should include:
  * **title** of the project
  * **authors** and their **contact info**
  * **short description** of their project
  * **prerequisites** for the user of the projects
  * **requirements and dependencies** including the necessary software or libraries and their versions
  * **installation instructions**, which may also refer to a separate installation file
  * **license text**, see an example [here](https://gitlab.com/qkitchen/qworld-documentation/-/blob/dda01f1b5e88a324931271fd57487c86771b0e93/templates/LICENSE_TEMPLATE.md)
  * **credits**, unless there is a separate file
  * other information
* **Remove all files that are not necessary for the users** in the final versions. For example, if you have some `.tex` files for generating pictures, please remove them before making merge requests.
* **Remove all cell outputs** for the jupyter notebooks. You can do this by `Kernel > Restart & Clear Output` or `Cell > All Output > Clear` or `open the command palette > clear all cells outputs` or via command `jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace <filename>`.
* **Follow the case-sensitive rules** when naming and referring the files since some of your users might use a case-sensitive operating system such as Linux OS, i.e., `Hello.txt` and `hello.txt` are different filenames in Linux OS.

Note that each project may have further rules defined by the maintainers/owners.

## Courses tips

Currently, the directory layout for courses is specified as follows:

```
short_project_name
├── README.md
├── Installation.md
├── index.ipynb
├── images
|   ├── image1.png
|   ├── image2.png
|   └── ...
├── notebooks
|   ├── 00_Credits.ipynb
|   ├── 01_Course_Part_One.ipynb
|   ├── 01_Course_Part_One_Solutions.ipynb
|   ├── 02_Another_Part_Of_Course.ipynb
|   ├── 02_Another_Part_Of_Course_Solutions.ipynb
|   └── ...
├── exercises
|   └── ...
|   └── ...
├── projects
|   └── ...
|   └── ...
└── ...
```
The `README.md` file should be prepared as described before. `Installation.md` should have at leeast a minimal installation guide so that anyone should be able to run `index.ipynb`. Some installation instructions may also be provided in the notebooks as the part of the course.

We suggest to have one main file, e.g., `index.ipynb`, to shortly introduce the course and to show the content. Whenever an user is lost somewhere in the materials, he or she should able to continue by returning to the main file.

`00_Credits.ipynb` should contain all credits, in which the main contributors must be listed with their contact info.

There is a lot of tips and details regarding the course layout, so we have prepared a [Minimal Working Example](), which You can use to start Your adventure with making a course.

## Modification

The coordinators of QKitchen may change both the guideline and referenced templates. 

We are open to any suggestions regarding guideline or referenced templates. 
Please contact us via qkitchen [at] qworld.lu.lv.