# Code of Conduct for events (workshops, conferences, hackathons, schools, and similar events)

Version: *January 01, 2020*

*This document should be adopted for each specific event by explicitly identifying the event organizers who will be in charge of dealing with any code of conduct issue.*

This isn't an exhaustive list of things that you should respect or can't do. Rather, take it in the spirit in which it's intended - a guide to make it easier to enrich all of us and the technical communities in which we participate [1].

All attendees, participants, mentors, educators, speakers, organizers, sponsors, and volunteers at our events are required to agree with the following Code of Conduct. Organisers will enforce this code throughout the event. We expect cooperation from all participants to help ensure a safe environment for everybody.

## The Quick Version

Our event is dedicated to providing a harassment-free *workshop/conference/hackathon* experience for everyone, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion (or lack thereof), or technology choices. We do not tolerate harassment of event participants in any form. Sexual language and imagery is not appropriate for any event venue, including talks, workshops, parties, Twitter and other online media. Event participants violating these rules may be sanctioned or expelled from the event.

We respect the minors (children under age 18) and we must make every effort to protect their rights. All private relationships, private communications (including social media channels), or sexual contacts with minors are prohibited.

The contact info of any attendee or participant cannot be requested by any mentor, educator, speaker, organizer, sponsor, or volunteer.

## The Less Quick Version

These behaviors (included but not limited) are prohibited during the event and its related activities:

*   any behavior endangering the health or safety of other participants of workshops
*   assaulting, harassing, intimidating, or threatening another participant of workshops
    *   Harassment includes offensive verbal comments related to gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion, technology choices, sexual images in public spaces, deliberate intimidation, stalking, following, harassing photography or recording, sustained disruption of talks or other events, inappropriate physical contact, and unwelcome sexual attention
*   stealing, misusing, destroying or damaging property belonging to the event organizers or the participants
*   supplying false information in the recruitment process for the event (including false parental consent)
*   any behavior that disrupts or interferes with the learning experience, including 
*   using offensive language or personal attacks
*   possessing or using every kind of weapons, explosives (including fireworks) or toxic or otherwise dangerous materials
*   participation in the event under the influence of alcohol, drugs or other stimulants

Participants asked to stop any harassing or prohibited behavior are expected to comply immediately.

Sponsors are also subject to the anti-harassment policy. In particular, sponsors should not use sexualised images, activities, or other material. Booth staff (including volunteers) should not use sexualised clothing/uniforms/costumes, or otherwise create a sexualised environment.

If a participant engages in harassing behavior, the event organisers may take any action they deem appropriate, including warning the offender or expulsion from the event.

If you are being harassed, notice that someone else is being harassed, or have any other concerns, **please contact a member of the event staff immediately**. Event staff can be identified as they'll be wearing branded clothing and/or badges. You may also directly contact the members of the Ethics Committee of QWorld.

Event staff will be happy to help participants contact venue security or local law enforcement, provide escorts, or otherwise assist those experiencing harassment to feel safe for the duration of the event. We value your attendance.

We expect participants to follow these rules at the event venues and event-related social events.

All event participants should be guided by several universal values: integrity, mutual respect, open-mindedness, discipline, tolerance, a sense of responsibility, and team spirit during our events.

## License and attribution

This Code of Conduct is directly adapted from the Conference Code of Conduct ([https://confcodeofconduct.com](https://confcodeofconduct.com)), which was originally sourced and credited as  [http://2012.jsconf.us/#/about](http://2012.jsconf.us/#/about) and [The Ada Initiative](http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy). The work is licensed under a Creative Commons Attribution 3.0 Unported License.

[1] Slack/Code of Conduct of Creative Commons [https://wiki.creativecommons.org/wiki/Slack/Code_of_Conduct](https://wiki.creativecommons.org/wiki/Slack/Code_of_Conduct)