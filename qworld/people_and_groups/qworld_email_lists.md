# QWorld's email lists

In this document, we list the e-mail lists used by QWorld members and groups. 

Please contact the given email address for more info. 

* The Board of QWorld (contact: info [at]qworld.lu.lv)
* The leading members of QWorld (contact: info [at]qworld.lu.lv)
* The representatives of QCousins (contact: qcousins [at] qworld.lu.lv)